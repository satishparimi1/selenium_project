package com.org.base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class BaseTest {

	public static WebDriver driver;
	
	
	public void launchBrowser(String browserName) {
		
		if(browserName.equalsIgnoreCase("Chrome")) {
			
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\Lib\\chromedriver.exe");
			driver = new ChromeDriver();
		}
		else {
			
		}
	}


	public WebDriver getDriver() {
		// TODO Auto-generated method stub
		return driver;
	}
}
