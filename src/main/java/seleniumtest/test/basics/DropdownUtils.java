package seleniumtest.test.basics;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class DropdownUtils {
	
	
	//Select an option from Drop down
	public void selectByVisibleText(WebElement dropdownLocator, String dropdownOptionVisibleText) throws InterruptedException {
		
		Select sel = new Select(dropdownLocator);
		sel.selectByVisibleText(dropdownOptionVisibleText);
		Thread.sleep(2000);
	}
	
	//2-nd priority method to select option from dropdown
	public void selectByValue(WebElement dropdownLoator, String value) {
		
	}
	
	//Never use this - 
	public void selectByIndex(WebElement dropdownLocator, int index) {
		
	}
	
	
	//NOTE: There is not de-selection of single select dropdown
	
	
	//Fetch all option from drop down
	public List<WebElement> getAllOptionsFromDropdown(WebElement dropdownLocator) {
		
		List<WebElement> dropdownOptions = new ArrayList();
		
		Select sel = new Select(dropdownLocator);
		
		dropdownOptions = sel.getOptions();
		
		return dropdownOptions;
	}
	
	
	//Selected options from multi-select dropdown
	public List<WebElement> getSelectedOptions(WebElement dropdownLocator){
		
		List<WebElement> selectedOptions = new ArrayList();
		
		Select sel = new Select(dropdownLocator);
		selectedOptions= sel.getAllSelectedOptions();
		
		return selectedOptions;
	}

	//Deselection of multi select dropdown
	public void deSelecttheOptionFromDropdown(WebElement dropdownLocator, String dropdownOption) {
		
		Select sel = new Select(dropdownLocator);
		sel.deselectByVisibleText(dropdownOption);
		
	}
	
	
	

}
