package seleniumtest.test.basics;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Demo {

	public static void main(String[] args) throws InterruptedException {
		
		
		//Launch new browser of Chrome
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\Lib\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		//Launch the website
		driver.get("https://www.linkedin.com/");
		System.out.println("test");
		
		//Maximize the screen
		driver.manage().window().maximize();
		
		//Hard waits - input is ms
		Thread.sleep(2000);
		
		//Navigate back
		driver.navigate().back();
		
		//CLOSE THE BROWSER	
		Thread.sleep(3000);
		driver.quit();
			
		

	}

}
