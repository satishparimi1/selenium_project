package com.org.winodwhandling;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Reporter;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners

public class WindowHandlingTest {
	
	
	/* CTRL + SHIFT + 0 */
	
	WebDriver driver;
	
	
	@Test
	public void validatePNRStats() throws InterruptedException {
		
		//Launch the browser
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\Lib\\chromedriver.exe");
		driver = new ChromeDriver();
		
		//Navigate to appliaction
		driver.get("https://www.irctc.co.in");
		driver.manage().window().maximize();
		
		
		
		//Click on PNR Status button
		try {
			driver.findElement(By.xpath("//label[contains(text(),'PNR STATUS')]")).click();
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			
			System.out.println("Exception: PNR Status button is not working");
			e.printStackTrace();
		}
		
		
		//Get Window ID's
		Set<String> winodw_IDs = 	driver.getWindowHandles();
		
		Iterator<String> itr = winodw_IDs.iterator();
		
		String parentTabID = itr.next();
		Reporter.log("Parent Tab ID is : "+parentTabID,true);
		
		String childTabID = itr.next();
		Reporter.log("Child Tab ID is : "+childTabID,true);
		
		
		//Switch the control to child tab
		driver.switchTo().window(childTabID);
		
		//Enter PNR Number
		driver.findElement(By.xpath("//input[@id='inputPnrNo']")).sendKeys("1234567890");
		Thread.sleep(2000);
		
		
		Reporter.log("PNR Status validation completed.",true);
		Thread.sleep(2000);
		
		driver.switchTo().window(parentTabID);
		
		driver.quit();
	}

	
	
	
	
}
