package com.org.actions;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ActionsTest {

	public static WebDriver driver;
	
	//It will run before execution of each test case
	@BeforeMethod
	public void openBrowser() {
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		
		//Launch browser
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\Lib\\chromedriver.exe");
		driver = new ChromeDriver(options);
		
		//Navigate to the application
		driver.get("https://www.irctc.co.in");
		
		//Maxmize the browser window
		driver.manage().window().maximize();
	}
	
	//It will execute before execution of after test case
	@AfterMethod
	public void closeBrowser() {
		
		//Close the browswer
		driver.quit(); //it will close all the tabs/windows launched in run time 
		
		//driver.close(); // it will close current active tab/window
	}
	
	
	
	@Test(priority=1)
	public void TC_01() throws InterruptedException {
	
		Reporter.log("Test_01 Execution Started..",true);
		
		
		//Mouse hover on Holidays Tab
		Actions act = new Actions(driver);
		
		WebElement holidays_tab = driver.findElement(By.xpath("//a[contains(text(),'HOLIDAYS')]"));
		
		act.moveToElement(holidays_tab).perform();
		Reporter.log("Hovered on Holidays Tab.",true);
		Thread.sleep(2000);
		
		//Hovering on Special Trains tab
		WebElement special_trains_tab = driver.findElement(By.xpath("//a[@class='a']//span[@class='list_text'][contains(text(),'Special Trains')]"));
		act.moveToElement(special_trains_tab).perform();
		Thread.sleep(2000);
		Reporter.log("Hovered on Special Trains Tab.",true);
		
		//Click on Maharaja express
		driver.findElement(By.xpath("//span[contains(text(),\"Maharaja's Express\")][1]")).click();
		Thread.sleep(2000);
		Reporter.log("Clicked on Maharaja Express",true);
		
		//Switch the control the maharaja express train tab
		
			//Get Window IDs
			Set<String> ids = driver.getWindowHandles();
			
			Iterator<String> itr  = ids.iterator();
			
			String parent_Tab_ID = itr.next();
			
			String child_Tab_ID = itr.next();
		
		
			//Switch the control to the Child Tab
			driver.switchTo().window(child_Tab_ID);
			Reporter.log("Driver control switched to child tab",true);
			Thread.sleep(1000);
			
			//Click on Book now button
			driver.findElement(By.xpath("//a[contains(text(),'Book Now')]")).click();
			Reporter.log("Clicked on Book Now button",true);
			Thread.sleep(2000);
		
	}
	
	/*@Test(priority=2)
	public void Tc_02() {
		Reporter.log("Test_02  Executed",true);
	}
	
	@Test(priority=3)
	public void Tc_03() {
		Reporter.log("Test_03  Executed",true);
	}*/
	
	
}
