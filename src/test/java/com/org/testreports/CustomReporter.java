package com.org.testreports;

import java.lang.reflect.InvocationTargetException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;

import com.org.base.BaseTest;
import com.relevantcodes.extentreports.LogStatus;

import test.utils.ExtentReports.ExtentTestManager;

public class CustomReporter extends BaseTest implements ITestListener, ISuiteListener{

	public WebDriver driverObj = BaseTest.driver;
	
	
	@Override
	public void onStart(ISuite suite) {
		// TODO Auto-generated method stub
		Reporter.log("Test Suite started : "+suite.getName(), true);
		
		
	}

	@Override
	public void onFinish(ISuite suite) {
		// TODO Auto-generated method stub
		Reporter.log("Test Suite stopped : "+suite.getName(), true);
		
		
	}
	
	@Override
	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub
		
		Reporter.log("Test Case started : "+result.getName(), true);
		
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub
		Reporter.log("Test Case passed : "+result.getName(), true);
		ExtentTestManager.getTest().log(LogStatus.PASS, result.getName()+" : is Passed");
	}

	@Override
	public void onTestFailure(ITestResult result) {
		// TODO Auto-generated method stub
		Reporter.log("Test Case Failed : "+result.getName(), true);
		
		
        ExtentTestManager.getTest().log(LogStatus.FAIL, result.getName()+" : is Passed");
		
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub
		Reporter.log("Test case skiped : "+result.getName(), true);
		
		ExtentTestManager.getTest().log(LogStatus.SKIP, result.getName()+" : is Skipped");
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
		Reporter.log("Test Context Started : "+context.getName(), true);
		
	}

	@Override
	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
		Reporter.log("Test Context stopped : "+context.getName(), true);
	}

	

	
}
