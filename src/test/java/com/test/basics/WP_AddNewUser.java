package com.test.basics;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WP_AddNewUser {

	public static WebDriver driver;
	
	public static void main(String[] args) {
		
		// Launch the Browser
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\Lib\\chromedriver.exe");
		driver = new ChromeDriver();

		// Maximize the browser

		driver.manage().window().maximize();

		// get()
		driver.get("https://s1.demo.opensourcecms.com/wordpress/wp-login.php");
		
		//Login to Application
		loginToApp();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		//Click on 	Users Tab in Left side panel
		//*[@id="menu-users"]/a/div[3]
		
		
		try {
			driver.findElement(By.xpath("//a[@href='users.php']")).click();
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			System.out.println("Navigated to Users step got failed ...!!");
		}
		

	}
	
	public static void loginToApp() {
		
		driver.findElement(By.id("user_login")).sendKeys("opensourcecms");
		driver.findElement(By.id("user_pass")).sendKeys("opensourcecms");
		driver.findElement(By.name("wp-submit")).click();
	}

}
