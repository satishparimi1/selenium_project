package com.test.basics;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.org.base.BaseTest;



public class IFSCCodeTest extends BaseTest {
	
	
	
	@Test
	public void IFSCCodeValidation() throws InterruptedException {
		
		/*System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") +"\\Lib\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		// Maximize the browser
		driver.manage().window().maximize();*/
		
		launchBrowser("Chrome");

		// get()
		driver.get("https://bankifsccode.com/");
		Thread.sleep(2000);
		
		/*DropdownUtils dropdowntest = new DropdownUtils();
		
		//Select Bank
		WebElement bankDropdown = driver.findElement(By.xpath("//form[@name='bank']//select[@name='SelectURL']"));
		dropdowntest.selectDropdownOptionUsingVisibleText(bankDropdown, "AXIS BANK");

		//Select State
		WebElement stateDropdown = driver.findElement(By.xpath("//form[@name='state']//select[@name='SelectURL']"));
		dropdowntest.selectDropdownOptionUsingVisibleText(stateDropdown, "TAMIL NADU");
		
		//Select District
		WebElement districtDropdown = driver.findElement(By.xpath("//form[@name='district']//select[@name='SelectURL']"));
		dropdowntest.selectDropdownOptionUsingVisibleText(districtDropdown, "CHENNAI");
		
		
		//Select Branch
		WebElement branchDropdown = driver.findElement(By.xpath("//form[@name='branch']//select[@name='SelectURL']"));
		dropdowntest.selectDropdownOptionUsingVisibleText(branchDropdown, "AVADI");
		
		//Get IFSC Code
		String IFSCCode = driver.findElement(By.xpath("//b[contains(text(),'IFSC Code:')]/following-sibling::a[1]")).getText();
		System.out.println("IFSC Code for Bank Of America - CHENNAI BRANCH :: "+IFSCCode);
		
		*/
		/*
		 * URL: https://www.swtestacademy.com/extentreports-testng/
		 */
		
		String browser_From_Jenkins = System.getProperty("browsername");
		
		System.out.println("Browser name received from Jenkins : "+browser_From_Jenkins);
		Reporter.log("Browser name received from Jenkins : "+browser_From_Jenkins, true);
		
		
		//Close the Browser
		driver.quit();
		
	}
	



}
