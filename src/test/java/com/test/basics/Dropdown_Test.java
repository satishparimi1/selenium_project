package com.test.basics;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;


/**
 * 
  	@author sparimi
  
  	Scenarios of Dropdown Testing in any application
  
  	1 - Validate dropdown is displaying or not
  	
  	2- Validate dropodown is in enabled state or not 
  	
  	3- Validate default option selected for dropdown
  
  	4- Validate all the options in the dropdown
  	
  	5- Validate user can able to select an option from drodown or not
  
  	6- Validate selected option for dropdown [Ex: Canada from Country dropdown]
  
  	7- Validate user can able to de-select ... already selected option
		   
 * 
 */
 

public class Dropdown_Test {

	public static void main(String[] args) {
		
		//Launch the Chrome Browser
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\Lib\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		// Maximize the browser
		driver.manage().window().maximize();

		// get()
		driver.get("file:///E:/selenium-stuff/Selenium%20Material/Day-13%20_%20Works%20with%20Dropdown/DropDown_Demo/Selenium%20Practice%20WebSite.html");
		
		
		//Identify Country Dropdown
		WebElement country_DD = driver.findElement(By.name("Countries"));
		
		//Create object for "Select" Class to work with dropdown
		Select Country_DD_Obj = new Select(country_DD);
		
		//Drop down - Default Option validation
		String defaultOPtion_Country_Dropdown = Country_DD_Obj.getFirstSelectedOption().getText();
		System.out.println("1 ===> Default Options selected for Country Dropdown : "+defaultOPtion_Country_Dropdown);
		
		
		//Select the "Canada" from Country Drop down
		Country_DD_Obj.selectByVisibleText("Canada");
		
		//Validate "Canada" got selected or not
		String selected_Option = Country_DD_Obj.getFirstSelectedOption().getText();
		System.out.println("2 ===> Selected Country for Country Dropdown : "+selected_Option);
		
		
		
		//Validate the list of Options
		List<WebElement> country_dropdown_options = Country_DD_Obj.getOptions();
		
		
		
		System.out.println();
		System.out.println("*** Following are the options available in Country Dropdown ***");
		
	
		//Using for-each loop get text from "List"
		for(WebElement element : country_dropdown_options) {
			System.out.println(element.getText());
		}
		
		//Close the browser after validation
		//driver.quit();
	}

}
