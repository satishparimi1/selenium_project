package com.test.basics;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class IRCTC_LoginTest {

	public static void main(String[] args) throws InterruptedException {
		
		
		//Launch the Browser
		
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\Lib\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		//Maximize the browser
		driver.manage().window().maximize();
		
		
		//get()
		driver.get("https://www.irctc.co.in");
		
		
		
		//2 - Identifying text box using Xpath
		boolean sourceStationTxtbox =	driver.findElement(By.xpath("//input[@placeholder='From*']")).isDisplayed();
		
		if(sourceStationTxtbox) {
			System.out.println("Source Station textbox is displaying in the UI");
		}else {
			System.out.println("Source Station textbox is - NOT - displaying in the UI");
		}
		
		
		/*boolean sourceStationTxtboxState =	driver.findElement(By.xpath("//input[@placeholder='From*']")).isDisplayed();
		
		if(sourceStationTxtboxState) {
			System.out.println("Source Station textbox is displaying in the UI");
		}else {
			System.out.println("Source Station textbox is - NOT - displaying in the UI");
		}*/
		
		//Select the "Flexibile with Date" checkbox and validate whether it is selected or not
		driver.findElement(By.xpath("//label[contains(text(),'Flexible With Date')]")).click();
		Thread.sleep(2000);
		
		boolean flexibleDateCheckboxState = driver.findElement(By.xpath("//label[contains(text(),'Flexible With Date')]")).isSelected();
		
		
		if(flexibleDateCheckboxState) {
			System.out.println("Flexible Date checkbox is selected ");
		}else {
			System.out.println("Flexible Date checkbox is NOT selected ");
		}
		
		//driver.quit();
		
		
		
		
		
		

	}

}
