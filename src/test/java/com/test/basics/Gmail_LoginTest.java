package com.test.basics;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Gmail_LoginTest {

	public static void main(String[] args) throws InterruptedException {

		// Launch the Browser

		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\Lib\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		// Maximize the browser

		driver.manage().window().maximize();

		// get()
		driver.get("https://accounts.google.com");
		
		
		
		
		//Identify the text
		
		String uiText = driver.findElement(By.xpath("//*[@id='headingSubtext']/span")).getText();
		
		System.out.println("*** UI Text is : **** "+uiText);
		
		
		
		
		//Getting the backend attribute values like Placeholders
		
		String attributeValue = driver.findElement(By.name("identifier")).getAttribute("aria-label");
		
		System.out.println("Attribute Value is : "+attributeValue);
		
		if(attributeValue.equalsIgnoreCase("Use your Google Account")) {
			System.out.println("Place holder test is passed ..");
		}else {
			System.out.println("Placeholder test case failed");
		}
		
		
		//Enter the email
		driver.findElement(By.name("identifier")).sendKeys("test@email.com");
		
		//Click on Next button
		driver.findElement(By.id("identifierNext")).click();
		
		Thread.sleep(1000); //Hard wait statements - we will not use this in real time - selenium wait statments 
		
		
		//Enter the password
		driver.findElement(By.name("password")).sendKeys("Test@123");
		
		
		//Click on Next button in Password page
		driver.findElement(By.id("passwordNext")).click();
		
	}

}
