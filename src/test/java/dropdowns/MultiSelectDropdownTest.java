package dropdowns;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import seleniumtest.test.basics.DropdownUtils;

public class MultiSelectDropdownTest {

	public static void main(String[] args) throws InterruptedException {
		
		//Launch the Chrome Browser
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\Lib\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		// Maximize the browser
		driver.manage().window().maximize();

		// get()
		driver.get("file:///E:/selenium-stuff/Selenium%20Material/Day-13%20_%20Works%20with%20Dropdown/DropDown_Demo/Selenium%20Practice%20WebSite.html");
				
		
		//Create an object for Dropdown Utils
		
		DropdownUtils ddUtils = new DropdownUtils();
		
		//Select multiple banks from "Banks Drop down"
		
		WebElement bankDropdown = driver.findElement(By.name("bank_usa"));
		ddUtils.selectByVisibleText(bankDropdown, "CitiCorp/CitiBank");
		Thread.sleep(2000);
		
		ddUtils.selectByVisibleText(bankDropdown, "Bank of America");
		Thread.sleep(2000);
		
		ddUtils.selectByVisibleText(bankDropdown, "California Federal Bank");
		
		
		
		//Validate selected options on Banks dropdown
		System.out.println("Selected banks are::");
		
		List<WebElement> banksSelected = ddUtils.getSelectedOptions(bankDropdown);
		
		for(WebElement ele : banksSelected) {
			
			System.out.println(ele.getText());
		}
		
		
		
		//De select the option from multi select drop down
		ddUtils.deSelecttheOptionFromDropdown(bankDropdown, "CitiCorp/CitiBank");
		
		
		
		
	}

	
}
