package dropdowns;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import seleniumtest.test.basics.DropdownUtils;

public class DropdownTest {

	public static void main(String[] args) throws InterruptedException {
		
		//Launch the Chrome Browser
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\Lib\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		// Maximize the browser
		driver.manage().window().maximize();

		// get()
		driver.get("file:///E:/selenium-stuff/Selenium%20Material/Day-13%20_%20Works%20with%20Dropdown/DropDown_Demo/Selenium%20Practice%20WebSite.html");
				
		
		//Create an object for Dropdown Utils
		
		DropdownUtils ddUtils = new DropdownUtils();
		
		
		//Selecting option from drop down
		WebElement countryDropdown = driver.findElement(By.name("Countries"));
		
		ddUtils.selectByVisibleText(countryDropdown, "Canada");
		
		
		
		//Validate Country drop down options
		System.out.println("**** Validating dropdown options ****");
		
		
		List<String> countries = new ArrayList();
		
		//Actual COUNTRIES LIst
		List<WebElement> actCountryOptions = ddUtils.getAllOptionsFromDropdown(countryDropdown);
		
		for(WebElement element : actCountryOptions) {
			countries.add(element.getText());
			System.out.println(element.getText());
		}
		
		
		List<String> countries_Exp_Result = new ArrayList();
		countries_Exp_Result.add("Select");
		countries_Exp_Result.add("United States");
		countries_Exp_Result.add("UK");
		countries_Exp_Result.add("Canada");
		countries_Exp_Result.add("Australia");
		countries_Exp_Result.add("India");
		countries_Exp_Result.add("China");
		countries_Exp_Result.add("Mexico");
		
		
		
		
		//Compare expected countries list with actual coutnries list
		if(countries.equals(countries_Exp_Result)) {
			
			System.out.println("Both Actual and Expected Country's list are same");
		}else {
			System.out.println("Countries are not matching in portal...!!");
		}
		
		
		
		
		
		
		
		
		
	}

	
}
